#Mac0110 - Miniep7
# Miqueias Vitor Lopes Lima - 11796340

function sin(x)
 i = 0
 n = 1
 serie = 0
  while n <= 21
    termo = ((-1)^i) * (x^n) / factorial(big(n))
    i = i + 1
    n = n + 2
    serie = serie + termo
  end
 return serie
end

function cos(x)
    i = 1
    n = 2
    serie = 1
  while n <= 21
    termo = ((-1)^i) * (x^n) / factorial(big(n))
    i = i + 1
    n = n + 2
    serie = serie + termo
  end
 return serie
end

function bernoulli(n)
 n *= 2
 A = Vector{Rational{BigInt}}(undef, n + 1)
   for m = 0 : n
   A[m + 1] = 1 // (m + 1)
    for j = m : -1 : 1
     A[j] = j * (A[j] - A[j + 1])
    end
   end
  return abs(A[1])
end

function tan(x)
  n = 2
  serie = x
    while n <= 10
     termo = (2^(2*n)) * (2^(2*n) - 1) * bernoulli(n) * (x^(2*n - 1)) / factorial(big(2*n))
     n = n + 1
     serie = serie + termo
    end
  return serie
end


function quaseigual(x, y)
  if abs(x - y) <= 0.0001
   return true
  else
   return false
  end
end

function check_sin(x, y)
  if quaseigual(x, sin(y))
     return true
   else 
    return false
  end
end

function check_cos(x, y)
  if quaseigual(x, cos(y))
     return true
   else
    return false
  end
end


function check_tan(x, y)
  if quaseigual(x, tan(y))
     return true
   else
    return false
  end
end

function taylor_sin(x)
 i = 0
 n = 1
 serie = 0
  while n <= 21
    termo = ((-1)^i) * (x^n) / factorial(big(n))
    i = i + 1
    n = n + 2
    serie = serie + termo
  end
 return serie
end

function taylor_cos(x)
    i = 1
    n = 2
    serie = 1
  while n <= 21
    termo = ((-1)^i) * (x^n) / factorial(big(n))
    i = i + 1
    n = n + 2
    serie = serie + termo
  end
 return serie
end

function taylor_tan(x)
  n = 2
  serie = x
    while n <= 10
     termo = (2^(2*n)) * (2^(2*n) - 1) * bernoulli(n) * (x^(2*n - 1)) / factorial(big(2*n))
     n = n + 1
     serie = serie + termo
    end
  return serie

function test()
  if !check_sin(0, 0)
   return "erro no sin"
  elseif !check_cos(1, 0)
   return "erro no cos"
  elseif !check_tan(0, 0)
   return "erro na tan"
  end
   return "final dos testes"
end

